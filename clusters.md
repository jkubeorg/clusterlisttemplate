
# Clusters

This table list essential settings of Kubernetes clusters.. 

NAME 			| STATE		| ZONE				| SETUP			| TEMPLATE				| CONTACTS
:---			| :---		| :---              | :---    		| :---					| :---
hello-world-1   | DELETED	| europe-west3-b  	| 				| jkube/helloworld		| 

*Remarks:*

The column STATE may contain the following values:

 *	DELETED  ==> cluster is not present, no resources allocated
 *	RUNNING ==> cluster is active, all required resources allocated
 *	PAUSED ==> cluster is inactive, no nodes running, persistent volumes and external ip address still allocated   
	
For a list of available values for the column ZONE (and the available machine types) see: https://cloud.google.com/compute/docs/regions-zones

The column SETUP gives the reference to the git repository (in same project as this repository) that contains 
the configuration setup for the cluster. Multiple clusters may use the same setup. If the 
repository does not exist, yet, it will be initialized with defaults, depending on the template.

The column TEMPLATE defines on which JKube template the cluster shall be based on. 
Beware: a change of template may cause a complete restart and cluster data might be lost!
For a list of currently available official JKube templates see: https://jkube.org/official-templates	

The owner of this repository will be informed by mail for all changes of the cluster specifications. 
In the column CONTACTS you may add additional email addresses (comma separated), which will be informed
in case of changes to the cluster in question.
